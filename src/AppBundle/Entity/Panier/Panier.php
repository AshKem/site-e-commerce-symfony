<?php

namespace AppBundle\Entity\Panier;

use Symfony\Component\Uid\Uuid;

use Doctrine\ORM\Mapping as ORM;
use \ArrayObject;


/**
 * Panier
 *@ORM\Entity
 */
class Panier
{
    /**
     * @var float
	 * @ORM\Column(name="total", type="float")
	 * 
     */
    private $total;

	/**
     * @var float
     */
    private $reduction;

	/**
	 * @var string
	 * @ORM\Column(name="id", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $refPanier;


    /**
     * @var ArrayObject
     */
    private $lignesPanier;

	public function __construct()
    {

		$this->lignesPanier = new ArrayObject();
		$this->reduction=1.0; //on est a 100%
    }

	
	public function setRefPanier($laRef)
	{	
		$this->refPanier=$laRef;
    }
	
	public function getRefPanier()
	{

		return $this->refPanier;
    }

	public function setTotal()
	{
		$this->recalculer();
    }
	
	public function getTotal()
	{
		$this->recalculer();
		return $this->total;
    }

	public function setReduction($reduc) {

		if($this->reduction!=1.0){ //Si 
			$alerte= "Vous avez déjà appliqué un code promo.";
			return $alerte;

		}else{
			
			$this->reduction=$this->reduction-$reduc;
			$pourcent=$reduc*100;
			$alerte= "Votre code promo est valide une reduction de ".$pourcent."% a été appliquée";
			return $alerte;
		}
		
	}
	
	public function getReduction()
	{
		return $this->reduction;
    }
	
	public function getLignesPanier() {
		return $this->lignesPanier;
	}
	
	public function recalculer() {
		$it = $this->getLignesPanier()->getIterator();
		$this->total = 0.0 ;
		while ($it->valid()) {
			$ligne = $it->current();
			$ligne->recalculer() ;
			$this->total += $ligne->getPrixTotal() ;
			$it->next();
		}
	}
	
	public function ajouterLigne($inArticle,$laTaille) {
		$lp = $this->chercherLignePanier($inArticle) ;
		if ($lp == null) {
			$lp = new LignePanier() ;
			$lp->setArticle($inArticle) ; 
			$lp->setTailleChoisie($laTaille) ; 
			$lp->setQuantite(1) ;
			$this->lignesPanier->append($lp) ;
		}
		else
			$lp->setQuantite($lp->getQuantite() + 1) ;
		$this->recalculer() ;
	}
	
	public function chercherLignePanier($inArticle) {
		$lignePanier = null ;
		$it = $this->getLignesPanier()->getIterator();
		while ($it->valid()) {
			$ligne = $it->current();
			if ($ligne->getArticle()->getRefArticle() == $inArticle->getRefArticle())
				$lignePanier = $ligne ;
			$it->next();
		}
		return $lignePanier ;
	}
	
	public function supprimerLigne($inRefArticle) {
		$existe = false ;
		$it = $this->getLignesPanier()->getIterator();
		while ($it->valid()) {
			$ligne = $it->current();
			if ($ligne->getArticle()->getRefArticle() == $inRefArticle) {
				$existe = true ;
				$key = $it->key();
			}
			$it->next();
		}
		if ($existe)
			$this->getLignesPanier()->offsetUnset($key);
	}
	
	public function viderPanier() {
		$this->lignesPanier->exchangeArray(array()) ;
	}

	
}

