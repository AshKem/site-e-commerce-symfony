<?php

namespace AppBundle\Entity\Panier;

use Doctrine\ORM\Mapping as ORM;
use \ArrayObject;
use \DateTime;

/**
 * Commande
 *
 * @ORM\Entity
 */
class Commande
{

	/**
     * @var integer
	 * @ORM\Id()
     * @ORM\GeneratedValue()
	 * @ORM\Column(name="idcommande", type="integer")
     */
    private $idCommande;

    /**
     * @var string
	 *  @ORM\Column(name="panier", type="string")
     */
    private $panier;

    /**
     * @var string
	 *  @ORM\Column(name="datecommande", type="string")
     */
    private $dateCommande;

    /**
     * @var string
	 *  @ORM\Column(name="datelivraison", type="string")
     */
    private $dateLivraison;

	/**
     * @var String
     *
     * @ORM\Column(name="etat", type="string")
     */
    private $etat;

	/**
     * @var string
	 *  @ORM\Column(name="nom", type="string")
     */
    private $nom;

	/**
     * @var string
	 *  @ORM\Column(name="prenom", type="string")
     */
    private $prenom;

	/**
     * @var string
	 *  @ORM\Column(name="adresse", type="string")
     */
    private $adresse;

	/**
     * @var string
	 *  @ORM\Column(name="ville", type="string")
     */
    private $ville;

	/**
     * @var string
	 *  @ORM\Column(name="codePostal", type="integer")
     */
    private $codePostal;


	public function __construct($lePanier)
    {
		$dateNow=new DateTime('NOW');

		$this->dateCommande = date_format($dateNow, 'Y-m-d H:i');
		$dateAfter = $dateNow->modify('+5 days');
		$this->dateLivraison = date_format($dateAfter, 'Y-m-d');
		$this->panier=$lePanier;
		$this->etat="En préparation";

    }

	public function setPanier($lePanier)
	{
		$this->panier=$lePanier;
  	}

	public function getPanier()
	{
		return $this->panier;
  	}

	public function setNom($leNom)
	{
		$this->nom=$leNom;
  	}

	public function getNom()
	{
		return $this->nom;
  	}

	public function setPrenom($lePrenom)
	{
		$this->prenom=$lePrenom;
  	}

	public function getPrenom()
	{
		return $this->prenom;
  	}

	public function setAdresse($lAdresse)
	{
		$this->adresse=$lAdresse;
  	}

	public function getAdresse()
	{
		return $this->adresse;
  	}

	public function setVille($laVille)
	{
		$this->ville=$laVille;
	}

	public function getVille()
	{
		return $this->ville;
	}

	public function setCodePostal($leCodePostal)
	{
		$this->codePostal=$leCodePostal;
  	}

	public function getCodePostal()
	{
		return $this->codePostal;
  	}


	public function getDateCommande()
	{
		return $this->dateCommande;
  	}
	
	/*public function setDateLivraison($jours)
	{
		$this->dateLivraison=$dateCommande+$jours;
  	}*/

	public function getDateLivraison()
	{
		return $this->dateLivraison;
  	}

	public function setEtat($laDateLivraison)
	{
		$this->panier=$lePanier;
	}

	public function getEtat()
	{
		return $this->etat;
	}


	public function getidCommande()
	{
		return $this->idCommande;
	}

}
