<?php

namespace AppBundle\Entity\Catalogue;

use Doctrine\ORM\Mapping as ORM;


/**
 * Livre
 *
 * @ORM\Entity
 */
class Pantalon extends Article
{

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     */
    private $description;

   

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    


    /**
     * Set description
     *
     * @param string $description
     *
     * @return Pantalon
     */
    public function setDescription($laDescription)
    {
        $this->description = $laDescription;

        return $this;
    }

    /**
     * Get description
     *
     * @return int
     */
    public function getDescription()
    {
        return $this->description;
    }



    



    /**
     * Set type
     *
     * @param string $leType
     *
     * @return Pantalon
     */
    public function setType($leType)
    {
        $this->type = $leType;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
