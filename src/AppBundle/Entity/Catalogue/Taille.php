<?php

namespace AppBundle\Entity\Catalogue;

use Doctrine\ORM\Mapping as ORM;

/**
 * Musique
 *
 * @ORM\Entity
 */
class Taille
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $refTaille;

    /**
     * @var string
     *
     * @ORM\Column(name="nomtaille", type="string")
     */
    private $nomTaille;
	
    /**
     * @ORM\ManyToOne(targetEntity="Article",cascade={"persist"},inversedBy="tailles")
     */
    private $article;

    /**
     * Set refTaille
     *
     * @param string $refTaille
     *
     * @return Taille
     */


    public function __construct($leNomTaille)
    {
      $this->nomTaille = $leNomTaille;
    }



    public function setRefTaille($refTaille)
    {
        $this->refTaille = $refTaille;

        return $this;
    }

    /**
     * Get refTaille
     *
     * @return integer
     */
    public function getRefTaille()
    {
        return $this->refTaille;
    }
	

    /**
     * Set nomTaille
     *
     * @param integer $nomTaille
     *
     * @return Taille
     */
    public function setNomTaille($nom)
    {
        $this->nomTaille = $nom;

        return $this;
    }

    /**
     * Get nomTaille
     *
     * @return string
     */
    public function getnomTaille()
    {
        return $this->nomTaille;
    }

    /**
     * Set article
     *
     * @param Article $leArticle
     *
     * @return Taille
     */
    public function setArticle($leArticle)
    {
        $this->article = $leArticle;

        return $this;
    }

    /**
     * Get article
     *
     * @return Taille
     */
    public function getArticle()
    {
        return $this->article;
    }
}

