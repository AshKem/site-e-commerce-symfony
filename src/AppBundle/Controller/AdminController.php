<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Psr\Log\LoggerInterface;

use Doctrine\ORM\EntityManagerInterface;

use AppBundle\Entity\Catalogue\Article;
use AppBundle\Entity\Catalogue\Livre;
use AppBundle\Entity\Catalogue\Pantalon;
use AppBundle\Entity\Catalogue\TeeShirt;
use AppBundle\Entity\Catalogue\Sweat;

class AdminController extends Controller
{
	private $entityManager;
	private $logger;
	
	/*
	Pb with codeanywhere.com
	public function __construct(EntityManagerInterface $entityManager)  {
		$this->entityManager = $entityManager;
	}*/
	
	public function init()  {
		$this->entityManager = $this->container->get('doctrine')->getEntityManager();
		$this->logger = $this->container->get('monolog.logger.php');
	}
	

	 /**
     * @Route("/admin/articles", name="adminArticles")
     */
    public function adminArticlesAction(Request $request)
    {
		$this->init() ;
		$query = $this->entityManager->createQuery("SELECT a FROM AppBundle\Entity\Catalogue\Article a");
		$articles = $query->getResult();
		return $this->render('admin.articles.html.twig', [
            'articles' => $articles,
        ]);
    }
	
	
    /**
     * @Route("/admin/articles/supprimer", name="adminArticlesSupprimer")
     */
    public function adminArticlesSupprimerAction(Request $request)
    {
		$this->init() ;
		$entityArticle = $this->entityManager->getReference("AppBundle\Entity\Catalogue\Article", $request->query->get("refArticle"));
		if ($entityArticle !== null) {
			$this->entityManager->remove($entityArticle);
			$this->entityManager->flush();
		}
		return $this->redirectToRoute("adminArticles") ;
    }

    /**
     * @Route("/admin/{article}/ajouter", name="adminArticlesAjouter")
     */
    public function adminArticlesAjouterAction(Request $request, string $article)
    {
		$this->init() ;

		//Selon le paramètre article contenu dans la route on créé un formulaire
		switch ($article) {
			case 'teeshirt': //Si on ajoute un Tee-shirt
				$entity = new TeeShirt() ;
				$formBuilder = $this->createFormBuilder($entity);
				$formBuilder->add("titre", TextType::class) ;
				$formBuilder->add("prix", NumberType::class) ;
				$formBuilder->add("disponibilite", IntegerType::class) ;
				$formBuilder->add("image", TextType::class) ;
				$formBuilder->add("type", TextType::class) ;
				$formBuilder->add("description", TextType::class) ;
				$formBuilder->add("valider", SubmitType::class) ;
				// Generate form
				$form = $formBuilder->getForm();
				
				$form->handleRequest($request) ;
				
				if ($form->isSubmitted()) {
					//On créé l'objet à partir des données saisies dans le formulaire
					$entity = $form->getData() ;
					$entity->setRefArticle(uniqid());
					$this->entityManager->persist($entity);
					$this->entityManager->flush();
					return $this->redirectToRoute("adminArticles") ;
				}
				else {
					return $this->render('admin.form.html.twig', [
						'form' => $form->createView(),
					]);
				}
				break;
			case 'pantalon': //Si on ajoute un Pantalon
				$entity = new Pantalon() ;
				$formBuilder = $this->createFormBuilder($entity);
				$formBuilder->add("titre", TextType::class) ;
				$formBuilder->add("prix", NumberType::class) ;
				$formBuilder->add("disponibilite", IntegerType::class) ;
				$formBuilder->add("image", TextType::class) ;
				$formBuilder->add("type", TextType::class) ;
				$formBuilder->add("description", IntegerType::class) ;
				$formBuilder->add("valider", SubmitType::class) ;
				// Generate form
				$form = $formBuilder->getForm();
				
				$form->handleRequest($request) ;
				
				if ($form->isSubmitted()) {
					//On créé l'objet à partir des données saisies dans le formulaire
					$entity = $form->getData() ;
					$entity->setRefArticle(uniqid());
					$this->entityManager->persist($entity);
					$this->entityManager->flush();
					return $this->redirectToRoute("adminArticles") ;
				}
				else {
					return $this->render('admin.form.html.twig', [
						'form' => $form->createView(),
					]);
				}
				break;
			case 'sweat': //Si on ajoute un Sweat
				$entity = new Sweat() ;
				$formBuilder = $this->createFormBuilder($entity);
				$formBuilder->add("titre", TextType::class) ;
				$formBuilder->add("prix", NumberType::class) ;
				$formBuilder->add("disponibilite", IntegerType::class) ;
				$formBuilder->add("image", TextType::class) ;
				$formBuilder->add("type", TextType::class) ;
				$formBuilder->add("description", IntegerType::class) ;
				$formBuilder->add("valider", SubmitType::class) ;
				// Generate form
				$form = $formBuilder->getForm();
				
				$form->handleRequest($request) ;
				
				if ($form->isSubmitted()) {
					//On créé l'objet à partir des données saisies dans le formulaire
					$entity = $form->getData() ;
					$entity->setRefArticle(uniqid());
					$this->entityManager->persist($entity);
					$this->entityManager->flush();
					return $this->redirectToRoute("adminArticles") ;
				}
				else {
					return $this->render('admin.form.html.twig', [
						'form' => $form->createView(),
					]);
				}
				break;
		}

    }
	

    /**
     * @Route("/admin/article/modifier", name="adminArticlesModifier")
     */
    public function adminArticlesModifierAction(Request $request)
    {
		$this->init() ;


		//Recupère la classe de l'objet qui correspond à la refArticle contenue dans la requête
		$em = $this->entityManager->getReference("AppBundle\Entity\Catalogue\Article", $request->query->get("refArticle"));
		$entityNom = get_class($em);
		echo $entityNom;

		//Selon la classe on récupère les infos dans une certaine classe
		switch ($entityNom) {
			case 'AppBundle\Entity\Catalogue\TeeShirt':
				$entity = $this->entityManager->getReference("AppBundle\Entity\Catalogue\TeeShirt", $request->query->get("refArticle"));
				break;
			case 'AppBundle\Entity\Catalogue\Pantalon':
				$entity = $this->entityManager->getReference("AppBundle\Entity\Catalogue\Pantalon", $request->query->get("refArticle"));
				break;
			case 'AppBundle\Entity\Catalogue\Sweat':
				$entity = $this->entityManager->getReference("AppBundle\Entity\Catalogue\Sweat", $request->query->get("refArticle"));
				break;
		}
		
		if ($entity !== null) {
			//On créé le formulaire en repranant les données contenu dans l'objet correspondant
			$formBuilder = $this->createFormBuilder($entity);
			$formBuilder->add("titre", TextType::class) ;
			$formBuilder->add("prix", NumberType::class) ;
			$formBuilder->add("disponibilite", IntegerType::class) ;
			$formBuilder->add("image", TextType::class) ;
			$formBuilder->add("type", TextType::class) ;
			$formBuilder->add("description", TextType::class) ;
			$formBuilder->add("valider", SubmitType::class) ;

			$form = $formBuilder->getForm();
			
			$form->handleRequest($request) ;
			
			if ($form->isSubmitted()) {
				//On applique les modifications
				$entity = $form->getData() ;
				$this->entityManager->persist($entity);
				$this->entityManager->flush();
				return $this->redirectToRoute("adminArticles") ;
			}
			else {
				return $this->render('admin.form.html.twig', [
					'form' => $form->createView(),
				]);
			}
		}
		else {
			return $this->redirectToRoute("adminArticles") ;
		}
    }

	
	

}
