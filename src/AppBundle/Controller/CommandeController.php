<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Psr\Log\LoggerInterface;

use Doctrine\ORM\EntityManagerInterface;

use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\ApaiIO;
use ApaiIO\Operations\Search;

use AppBundle\Entity\Catalogue\Sweat;
use AppBundle\Entity\Catalogue\Pantalon;
use AppBundle\Entity\Catalogue\TeeShirt;
use AppBundle\Entity\Panier\Commande;
use AppBundle\Entity\Panier\Panier;
use AppBundle\Entity\Catalogue\Taille;


class CommandeController extends Controller
{
	private $entityManager;
	private $panier;
	private $logger;
	private $paniers;
	
	/*
	Pb with codeanywhere.com
	public function __construct(EntityManagerInterface $entityManager)  {
		$this->entityManager = $entityManager;
	}*/
	
	public function init()  {
		$this->entityManager = $this->container->get('doctrine')->getEntityManager();
		$this->logger = $this->container->get('monolog.logger.php');
	}

	/**
     * @Route("/remplirInformations", name="remplirInformations")
     */
    public function remplirInformationsAction(Request $request){

		$this->init() ;

		$session = $request->getSession() ;
		$this->panier = $session->get("panier") ;
		$entity = new Commande($this->panier->getRefPanier());

				$formBuilder = $this->createFormBuilder($entity);
				$formBuilder->add("nom", TextType::class) ;
				$formBuilder->add("prenom", TextType::class) ;
				$formBuilder->add("adresse", TextType::class) ;
				$formBuilder->add("ville", TextType::class) ;
				$formBuilder->add("codePostal", IntegerType::class) ;
				$formBuilder->add("valider", SubmitType::class) ;
				// Generate form
				$form = $formBuilder->getForm();
				
				$form->handleRequest($request) ;
				
				if ($form->isSubmitted()) {
					//On créé l'objet à partir des données saisies dans le formulaire
					$entity = $form->getData() ;
					$this->entityManager->persist($entity);
					$this->entityManager->flush();
					$session->remove("panier") ;
					return $this->redirectToRoute("commanderPanier") ;
				}
				else {
					return $this->render('form.html.twig', [
						'form' => $form->createView(),
					]);
				}
	

		
    }

	/**
     * @Route("/commanderPanier", name="commanderPanier")
     */
    public function commanderPanierAction(Request $request)
    {
		$this->init() ;


		return $this->render('commande-passee.html.twig');
		
		
    }
    /**
     * @Route("/consulterCommandes", name="consulterCommandes")
     */
    public function consulterCommandeAction(Request $request)
    {
		$this->init() ;
		
		$query = $this->entityManager->createQuery("SELECT a FROM AppBundle\Entity\Panier\Commande a");

		$commandes = $query->getResult();

		
			/*	$commande = new Commande();
				$commande->setPanier();
				$commande->setEtat("En préparation pour l'expédition");
				
				$this->entityManager->persist($commande);
				$this->entityManager->flush();*/

				return $this->render('commande.html.twig', [
					'commandes' => $commandes,
    
				]);
    }


}