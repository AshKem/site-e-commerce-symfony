<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Psr\Log\LoggerInterface;

use Doctrine\ORM\EntityManagerInterface;

use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\ApaiIO;
use ApaiIO\Operations\Search;


use AppBundle\Entity\Catalogue\Livre;
use AppBundle\Entity\Catalogue\Sweat;
use AppBundle\Entity\Catalogue\Pantalon;
use AppBundle\Entity\Catalogue\TeeShirt;

class ArticleController extends Controller
{
	private $entityManager;
    private $article;

	public function __construct(EntityManagerInterface $entityManager)  {
		$this->entityManager = $entityManager;
	}

    /**
     * @Route("/consulterArticle", name="consulterArticle")
     */
    public function consulterArticleAction(Request $request)
    {
		
		$this->article = $this->entityManager->getReference("AppBundle\Entity\Catalogue\Article", $request->query->get("refArticle"));
		
		return $this->render('article.html.twig', [
            'article' => $this->article,
        ]);
    }


}
