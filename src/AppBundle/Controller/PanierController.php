<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Psr\Log\LoggerInterface;

use AppBundle\Entity\Catalogue\Article;
use AppBundle\Entity\Panier\Panier;
use AppBundle\Entity\Panier\LignePanier;
use AppBundle\Entity\Panier\Commande;

use Doctrine\ORM\EntityManagerInterface;

class PanierController extends Controller
{
	private $entityManager;
	private $panier;
	private $logger;
	private $paniers;
	
	/*
	Pb with codeanywhere.com
	public function __construct(EntityManagerInterface $entityManager)  {
		$this->entityManager = $entityManager;
	}*/
	
	public function init()  {
		$this->entityManager = $this->container->get('doctrine')->getEntityManager();
		$this->logger = $this->container->get('monolog.logger.php');
	}
	


    /**
     * @Route("/ajouterLigne", name="ajouterLigne")
     */
    public function ajouterLigneAction(Request $request)
    {
		$this->init() ;

		$session = $request->getSession() ;
		if (!$session->isStarted())
			$session->start() ;	
		if ($session->has("panier")){
			$this->panier = $session->get("panier") ;

		}else{
			$this->panier = new Panier() ;
			$this->panier->setRefPanier(uniqid());
			
		}
		
		if(!empty($_POST['tailles'])) {
			$taille = $_POST['tailles'];
		} 
		
		$article = $this->entityManager->getReference("AppBundle\Entity\Catalogue\Article", $request->query->get("refArticle"));
		$this->panier->ajouterLigne($article,$taille) ;
		$session->set("panier", $this->panier) ;

		

		return $this->render('panier.html.twig', [
            'panier' => $this->panier,
        ]);
    }
	
    /**
     * @Route("/supprimerLigne", name="supprimerLigne")
     */
    public function supprimerLigneAction(Request $request)
    {
		$this->init() ;
		$session = $request->getSession() ;
		if (!$session->isStarted())
			$session->start() ;	
		if ($session->has("panier"))
			$this->panier = $session->get("panier") ;
		else{
			$this->panier = new Panier() ;
			$this->panier->setRefPanier(uniqid());
		}

		$this->panier->supprimerLigne($request->query->get("refArticle")) ;
		$session->set("panier", $this->panier) ;
		if (sizeof($this->panier->getLignesPanier()) === 0)
			return $this->render('panier.vide.html.twig');
		else
			return $this->render('panier.html.twig', [
				'panier' => $this->panier,
			]);
    }
	
    /**
     * @Route("/recalculerPanier", name="recalculerPanier")
	 * @Method({"GET", "POST"})
     */
    public function recalculerPanierAction(Request $request)
    {
		$this->init() ;
		$session = $request->getSession() ;
		if (!$session->isStarted())
			$session->start() ;	
		if ($session->has("panier"))
			$this->panier = $session->get("panier") ;
		else{
			$this->panier = new Panier() ;
			$this->panier->setRefPanier(uniqid());
		}
			
		$it = $this->panier->getLignesPanier()->getIterator();
		while ($it->valid()) {
			$ligne = $it->current();
			$article = $ligne->getArticle() ;
			$ligne->setQuantite($request->request->get("cart")[$article->getRefArticle()]["qty"]);
			$ligne->recalculer() ;
			$it->next();
		}
		$this->panier->recalculer() ;
		$session->set("panier", $this->panier) ;
		return $this->render('panier.html.twig', [
            'panier' => $this->panier,
        ]);
    }

	/**
     * @Route("/ajouterCodePromo", name="ajouterCodePromo")
	 * @Method({"GET", "POST"})
     */
    public function ajouterCodePromoAction(Request $request)
    {
		$alerte=null;
		$this->init() ;
		$session = $request->getSession() ;
		if (!$session->isStarted())
			$session->start() ;	
		if ($session->has("panier"))
			$this->panier = $session->get("panier") ;
		else{
			$this->panier = new Panier() ;
			$this->panier->setRefPanier(uniqid());
		}
		
		$codePromo=$request->query->get("codePromo");
		
		if($codePromo=="PROMO10"){
			$reduc=0.1;
			return $this->render('panier.html.twig', [
				'panier' => $this->panier,
				'alerte' => $this->panier->setReduction($reduc),
			]);
			
		}else{
			$alerte = "Le code promo est invalide";
		}
		
		$session->set("panier", $this->panier) ;
		return $this->render('panier.html.twig', [
            'panier' => $this->panier,
			'alerte' => $alerte,
        ]);
    }
	
    /**
     * @Route("/accederAuPanier", name="accederAuPanier")
     */
    public function accederAuPanierAction(Request $request)
    {
		$this->init() ;
		$session = $request->getSession() ;
		if (!$session->isStarted())
			$session->start() ;	
		if ($session->has("panier"))
			$this->panier = $session->get("panier") ;
		else{
			$this->panier = new Panier() ;
			$this->panier->setRefPanier(uniqid());
		}
		
		if (sizeof($this->panier->getLignesPanier()) === 0)
			return $this->render('panier.vide.html.twig');
		else
			return $this->render('panier.html.twig', [
				'panier' => $this->panier,
			]);
    }

	
	
    
}
