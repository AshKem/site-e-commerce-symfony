<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Psr\Log\LoggerInterface;

use Doctrine\ORM\EntityManagerInterface;

use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\ApaiIO;
use ApaiIO\Operations\Search;

use AppBundle\Entity\Catalogue\Sweat;
use AppBundle\Entity\Catalogue\Pantalon;
use AppBundle\Entity\Catalogue\TeeShirt;
use AppBundle\Entity\Catalogue\Taille;

class RechercheController extends Controller
{
	private $entityManager;

	public function __construct(EntityManagerInterface $entityManager)  {
		$this->entityManager = $entityManager;
	}

    /**
     * @Route("/afficheRecherche", name="afficheRecherche")
     */
    public function afficheRechercheAction(Request $request, LoggerInterface $logger)
    {
		$this->initVetements() ;
		$query = $this->entityManager->createQuery("SELECT a FROM AppBundle\Entity\Catalogue\Article a");
    	//$query = $this->entityManager->createQuery("SELECT a FROM AppBundle\Entity\Catalogue\Vetements a");

		$articles = $query->getResult();
		return $this->render('recherche.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/afficheRechercheParMotCle", name="afficheRechercheParMotCle")
     */
    public function afficheRechercheParMotCleAction(Request $request, LoggerInterface $logger)
    {
		$this->initVetements() ;
		//$query = $this->entityManager->createQuery("SELECT a FROM AppBundle\Entity\Catalogue\Article a "
		//										  ." where a.titre like :motCle");
		//$query->setParameter("motCle", "%".$request->query->get("motCle")."%") ;
		$query = $this->entityManager->createQuery("SELECT a FROM AppBundle\Entity\Catalogue\Article a "
												  ." where a.titre like '%".addslashes($request->query->get("motCle"))."%'");
		$articles = $query->getResult();
		return $this->render('recherche.html.twig', [
            'articles' => $articles,
        ]);
    }

	private function initVetements() {
		if (count($this->entityManager->getRepository("AppBundle\Entity\Catalogue\Article")->findAll()) == 0) {

			$taille36= new Taille("36");
			$taille38= new Taille("38");
			$taille40= new Taille("40");
			$taille42= new Taille("42");
			$taille44= new Taille("44");
			$taille46= new Taille("46");

			$tailleS= new Taille("S");
			$tailleM= new Taille("M");
			$tailleL= new Taille("L");
			$tailleXL= new Taille("XL");
			

				$Sweat = new Sweat();
				$Sweat->setRefArticle(uniqid());
				$Sweat->setTitre("Sweat rose");
				$Sweat->setDescription("Un sweat pour femme rose");
				$Sweat->setType("Sweat à capuche");
				$Sweat->setPrix("14.90");
				$Sweat->setDisponibilite(1);
				$Sweat->setImage("/images/sweat.jpg");
				$Sweat->addTaille($tailleS);
				$Sweat->addTaille($tailleM);
				$Sweat->addTaille($tailleXL);
				$this->entityManager->persist($Sweat);
				$this->entityManager->flush();

				$Pantalon = new Pantalon();
				$Pantalon->setRefArticle(uniqid());
				$Pantalon->setTitre("Pantalon gris");
				$Pantalon->setDescription("Un pantalon pour femme gris");
				$Pantalon->setType("Jean");
				$Pantalon->setPrix("19.90");
				$Pantalon->setDisponibilite(1);
				$Pantalon->setImage("/images/pantalon.jpg");
				$Pantalon->addTaille($taille36);
				$Pantalon->addTaille($taille38);
				$Pantalon->addTaille($taille40);
				$Pantalon->addTaille($taille44);
				$this->entityManager->persist($Pantalon);
				$this->entityManager->flush();

				$TeeShirt = new TeeShirt();
				$TeeShirt->setRefArticle(uniqid());
				$TeeShirt->setTitre("Tee-shirt blanc");
				$TeeShirt->setDescription("Un Tee-shirt homme blanc");
				$TeeShirt->setType("Tee-shirt manches courtes");
				$TeeShirt->setPrix("9.90");
				$TeeShirt->setDisponibilite(1);
				$TeeShirt->setImage("/images/teeshirt.jpg");
				$TeeShirt->addTaille($tailleM);
				$TeeShirt->addTaille($tailleL);
				$this->entityManager->persist($TeeShirt);
				$this->entityManager->flush();
		}
		
	}


    public function removeArticle($id)
    {
        $entityManager = $this->entityManager;

		$query = $entityManager->createQuery("DELETE FROM AppBundle\Entity\Catalogue\Article a WHERE a.refArticle = " .$id);
		$query->execute();

    }
}
